# Malou - Product Hunt / Test

Ce projet comporte un repertoire backend pour le backend développé avec Node JS (NPM v6.13.6 & Node v13.7.0) et un repertoire frontend pour le frontend développé avec Angular 9.1.6.

## Demo du projet

Une demo du projet est disponible à l'adresse https://malou.djopa.fr.

## Cloner le projet

Pour pouvoir tester le projet, il faudrait le cloner depuis Git. 

```sh
git clone https://gitlab.com/wdjopa/malouphtest
```

Une fois le projet cloné, se rendre dans chacun des repertoires **backend** et **frontend** et lancer la commande
 
> npm install
 
 Afin d'installer toutes les dépendandes pour le serveur Express et le framework Angular.



## Repertoire Backend / NodeJS-Express

Pour lancer le serveur backend, il faudrait s'assurer des valeurs présentes dans le fichier `./backend/.env`.

Si tout est correct, lancer la commande 
```sh
npm start
```
 afin de lancer le serveur nodejs.

> Note : L'API utilisée pour faire la requête est l'API v2 de Product Hunt. Elle utilise sur GraphQL([Doc Product Hunt](https://api.producthunt.com/v2/docs))

## Repertoire Frontend / Angular

### Lancement du projet

Pour lancer le projet en développement, il faut lancer la commande `ng serve`. Il faudra ensuite se rendre à l'adresse `http://localhost:4200/`.

La page qui est présentée est la seule page de l'application. Elle a été développée dans le composant app.component.

Le projet utilise [Material UI Angular](https://material.angular.io/) & [NgxBootstrap](https://valor-software.com/ngx-bootstrap).


### Fonctionnement/Utilisation

Sur la page, un datepicker permet de renseigner un intervalles de dates pour faire remonter les produits.
20 produits sont remontés par page. Une fois l'utilisation en bas de la page, d'autres produits sont remontées au fur et à mesure.


### Lancement des tests

Pour les tests unitaires, lancer la commande `ng test`

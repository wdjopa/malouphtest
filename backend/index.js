const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const fetch = require("node-fetch");
const env = require("dotenv");
const cors = require("cors");
const fs = require("fs");
const http = require("http");
const https = require("https");

env.config();
app.use(bodyParser.json());
app.use(cors());

const http_port = process.env.HTTP_PORT;
const https_port = process.env.HTTPS_PORT;
const baseURL = process.env.PH_URL;

app.post("/getProductsPerDaterange", (req, res) => {
  let dates = req.body.dates;
  let next = req.body.next ? req.body.next : "";
  let perPage = req.body.perPage ? req.body.perPage : 20;
  const opts = {
    headers: {
      Authorization: `Bearer ${process.env.ACCESS_TOKEN}`,
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": process.env.FRONTEND_URL,
    },
    method: "POST",
    mode: "cors",
    body: JSON.stringify({
      query: `
        query { 
            posts(first: ${perPage}, after: "${next}", postedAfter : "${dates[0]}", postedBefore : "${dates[1]}"){
                pageInfo{
                    hasNextPage
                    hasPreviousPage
                    startCursor
                    endCursor
                }
                totalCount 
                edges{
                    cursor
                    node{
                        id
                        name
                        createdAt
                        tagline
                        description
                        url
                        votesCount
                        collections {
                            edges{
                                node{
                                    name
                                }
                            }
                        }
                        thumbnail{
                            type 
                            url
                        }
                        website
                        reviewsRating
                    }
                }
            }
        }`,
    }),
  };

  fetch(baseURL, opts)
    .then((res) => {
      return res.json();
    })
    .then((json) => {
      res.send(json);
    })
    .catch((err) => {
      res.status(400).send({
        message: "This is an error!",
      });
    });
});

app.get("/", (req, res) => {
  res.send("Le serveur est lancé !");
});

// app.listen(port, () => {
//   console.log(`Serveur backend lancé à l'adresse http://localhost:${port}`);
// });
// your express configuration here
const httpServer = http.createServer(app);
httpServer.listen(http_port, ()=>{
    console.log(`Serveur backend lancé en http`)
});

if(process.env.PROD == 'true'){
    const privateKey = fs.readFileSync(process.env.PRIV_KEY, "utf8");
    const certificate = fs.readFileSync(process.env.CERT, "utf8");
    const credentials = { key: privateKey, cert: certificate };
    const httpsServer = https.createServer(credentials, app);
    httpsServer.listen(https_port, () => {
      console.log(`Serveur backend lancé en https`);
    });
}

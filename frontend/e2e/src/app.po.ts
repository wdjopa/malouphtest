import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  async selectDate(date: Date) {
    await element(by.css('date-range')).sendKeys([date.toString(), date.toString()].join('-'));
  }

  // getTitleText(): Promise<string> {
  //   return element(by.css('app-root header .title')).getText() as Promise<string>;
  // }
}

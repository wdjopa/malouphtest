import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Affichage du input daterange', () => {
    page.navigateTo();
    const date = new Date();
    expect(page.selectDate(date)).toEqual([date.toString(), date.toString()].join('-'));
    // expect(page.getTitleText()).toEqual('date-range');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

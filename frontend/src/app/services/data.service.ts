import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient) { }

  /**
   * Lances une requête au serveur Node pour récupérer les produits en fonction de la daterange.
   * Si un curseur est présent, il récupère les données à partir de ce curseur
   * @param range Intervalle de date
   * @param cursor Curseur du dernier produit affiché pour permettre de charger les produits suivants
   */
  getProducts(range: string[], cursor: string) {
    return this.http.post(environment.url + '/getProductsPerDaterange', { dates: range, next: cursor });
  }

}

import { TruncatePipe } from './truncate.pipe';

describe('TruncatePipe', () => {

  const pipe = new TruncatePipe();

  it('Crée une instance', () => {
    expect(pipe).toBeTruthy();
  });

  it(`Transforme 'bonjour' en 'bonj...'`, () => {
    expect(pipe.transform('bonjour', [4])).toBe('bonj...');
  });

  it(`Transforme 'bonjour' en 'bonj---'`, () => {
    expect(pipe.transform('bonjour', [4, '---'])).toBe('bonj---');
  });
});

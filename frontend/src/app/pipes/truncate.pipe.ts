import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {

  /**
   * Tronc une chaine de caractère.
   * @param value chaine de caractères
   * @param args paramètres envoyés args[0] c'est la longueur maximale souhaitée et args[1] est le motif de troncature (par ex : ...)
   */
  transform(value: string, args: any[]): string {
    const limit = args.length > 0 ? parseInt(args[0], 10) : 20;
    const trail = args.length > 1 ? args[1] : '...';
    return value.length > limit ? value.substring(0, limit) + trail : value;
  }

}

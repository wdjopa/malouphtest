import { Component, HostListener } from '@angular/core';
import { DataService } from './services/data.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public collections = {};
  public dates: string[];
  private cursor: string;
  public totalProduits = 0;
  public range: Date[];
  public datas: any;
  public isLoading = false;
  public chart = {
    title: 'Produits par catégories',
    type: 'PieChart',
    columnNames: ['Catégorie', 'Nombre de produits'],
    options: {},
    data: [],
    height: 400
  };


  constructor(private dataService: DataService, private snackBar: MatSnackBar) { }

  /**
   * Appellée lorsque la valeur du datepicker a changé
   * @param rangeDate valeur récupérer du datepicker
   */
  onValueChange(rangeDate) {
    this.range = rangeDate;
  }

  /**
   * Déclenchée lors du clic sur le bouton 'valider' du datepicker
   */
  search() {
    if (this.range) {
      this.snackBar.open('Recherche des produits', 'FERMER', { duration: 5000 });
      // On change le format de la date
      this.dates = this.range.map(date => this.formatDate(date));
      this.isLoading = true;
      this.datas = null;
      this.getProducts(null);
    }
  }

  /**
   * Met à jour le PieChart des collections avec les données entrées
   */
  updateCollectionChart() {
    this.chart.data = [];
    // On met à jour les données dans le chart
    for (const [key, value] of Object.entries(this.collections)) {
      this.chart.data.push([key, value]);
    }
  }

  /**
   * Lances la methode getProducts du DataService pour l'afficher sur la page.
   * @param dates le tableau de dates(debut, fin) sous forme YYYY-MM-DD
   * @param cursor indice (chaine de caractere) du dernier élement pour pouvoir charger les elements qui le suivent
   */
  getProducts(cursor: string) {
    this.dataService.getProducts(this.dates, cursor).subscribe((result: any) => {
      this.isLoading = false;
      this.snackBar.dismiss();
      if (result.errors) {
        this.snackBar.open(`Erreur : ${result.errors[0].error_description}`, 'FERMER', { duration: 5000 });
      } else {
        // Update du total des produits trouvés
        this.totalProduits = result.data.posts.totalCount;
        // Si un page suit, on enregistre le dernier curseur pour l'eventuelle prochaine requete
        if (result.data.posts.pageInfo.hasNextPage) {
          this.cursor = result.data.posts.pageInfo.endCursor;
        }
        // On recupère les collections des produits pour le PieChart
        let collectionsArr = [];
        result.data.posts.edges.forEach((post: any) => {
          if (post.node.collections) {
            post.node.collections.edges.forEach(collection => {
              if (!collectionsArr.includes(collection.node.name.toLowerCase())) {
                collectionsArr.push(collection.node.name.toLowerCase());
              }
            });
          }
        });
        // On retire les doublons
        collectionsArr = Array.from(new Set(collectionsArr));

        // On regarde le nombre de produits étant dans chacune des collections.
        collectionsArr.forEach(collection => {
          result.data.posts.edges.forEach((post: any) => {
            if (post.node.collections) {
              const postCols = post.node.collections.edges.map(col => col.node.name.toLowerCase());
              if (postCols.includes(collection)) {
                if (this.collections[collection]) {
                  this.collections[collection]++;
                } else {
                  this.collections[collection] = 1;
                }
              }
            }
          });
        });

        // On met à jour les données à afficher sur la page
        if (this.datas) {
          this.datas.edges.push(...result.data.posts.edges);
        } else {
          this.datas = result.data.posts;
        }

        // On appelle la méthode pour mettre à jour le PieChart
        this.updateCollectionChart();
      }
    }, (error) => {
      this.isLoading = false;
      this.snackBar.open(`Erreur : La connexion au serveur a échoué !`, 'FERMER', { duration: 5000 });
    });
  }

  /**
   * Charge de nouvelles données (produits) lorsque le scroll est en bas de page
   */
  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    const pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    const max = document.documentElement.scrollHeight;
    if (pos >= (max - 500) && !this.isLoading && this.range && this.datas.pageInfo.hasNextPage) {
      this.isLoading = true;
      this.getProducts(this.cursor);
    }
  }

  /**
   * Retourne une date au format YYYY-MM-DD
   * @param date Date
   */
  formatDate(date) {
    const d = new Date(date);
    const year = d.getFullYear();
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();

    month = (month.length < 2) ? '0' + month : month;
    day = (day.length < 2) ? '0' + day : day;

    return [year, month, day].join('-');
  }

}

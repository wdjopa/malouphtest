import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DataService } from './services/data.service';

describe('AppComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: DataService, useValue: new DataService(null) }, { provide: MatSnackBar, useValue: null }],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));


  it(`Le totalProduit initial doit être à 0`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.totalProduits).toEqual(0);
  });

  it(`Le isLoading initial doit être à false`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.isLoading).toBeFalse();
  });


  it(`Il doit y avoir une chart de type piechart`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.chart.type).toEqual('PieChart');
  });


  // it(`Lorsqu'on lance une recherche`, () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.componentInstance;
  //   const d1 = new Date();
  //   app.range = [d1, d1];
  //   app.search();
  //   expect(app.dates).toEqual([app.formatDate(d1), app.formatDate(d1)]);
  //   expect(app.isLoading).toBeTrue();
  // });


  it('La date doit avoir le format YYYY-MM-DD', () => {
    const d = new Date();
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.formatDate(d.toString())).toBe(`${d.getFullYear()}-${((d.getMonth() + 1) > 9 ? (d.getMonth() + 1) : '0' + (d.getMonth() + 1))}-${(d.getDate() > 9) ? d.getDate() : '0' + d.getDate()}`);
  });
});
